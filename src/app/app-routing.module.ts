import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { RegionComponent } from './components/region/region.component';
import { CountryDetailsComponent } from './components/country-details/country-details.component';


const routes: Routes = [
  { path: 'home', component: RegionComponent },
  { path: 'countryDetails', component: CountryDetailsComponent},
  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
