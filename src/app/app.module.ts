import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';

import { TableComponent } from './components/table/table.component';

import { TableService } from './core/services/table.service';
import { PagerService } from './core/services/pager.service';

@NgModule({
  declarations: [
    AppComponent,
    TableComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [TableService, PagerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
