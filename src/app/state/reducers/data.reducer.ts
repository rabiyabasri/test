import * as fromData from '../actions/data.actions';

import { Region, Countries } from '../../state/models/region';

export interface DataState {
  regions: Region[];
  region: string;
  loading: boolean;
  error: any;
  results: Countries[];
}
export const initialState: DataState = {
  regions: [
    { viewValue: 'Europe', value: 'europe' },
    { viewValue: 'Asia', value: 'asia' }
  ],
  region: '',
  loading: false,
  error: null,
  results: [],
};
export function reducer(state = initialState, action: fromData.ActionsUnion): DataState {
  switch (action.type) {
    case fromData.REGION: {
      return {
          ...state,
          loading: false,
          region: action.payload
      };
  }
    case fromData.COUNTRY_SUCCESS: {
        return {
            ...state,
            loading: false,
            results: action.payload
        };
    }

    case fromData.COUNTRY_FAILURE: {
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };
    }

    default: {
      return state;
    }
  }
}

export const getItems = (state: DataState) => state.results;
export const getRegionDetails = (state: DataState) => state.regions;
