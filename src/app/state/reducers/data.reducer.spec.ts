
import { reducer, initialState, getItems, getRegionDetails  } from './data.reducer';
import * as fromData from '../actions/data.actions';

describe('Data Reducer', () => {
  it('unknown action should return the previous state', () => {
    const action = { type:'NOOP' } as any;
    const result = reducer(initialState, action);
    expect(result).toBe(initialState);
  });
  it('should check for region', () => {
    const payLoad = 'asia';
    const action = new fromData.Region(payLoad);
    const result = reducer(initialState, action);
    expect(result).toEqual({...initialState,region:action.payload});
  });
  it('should populate country data', () => {
    const payLoad = 
      [{ name: 'Afghanistan', capital: 'Kabul', population: 1, currencies: 'Afghan afghani', 
      flag: 'https://restcountries.eu/data/afg.svg' }];
    const action = new fromData.CountrySuccess(payLoad);
    const result = reducer(initialState, action);
    expect(result).toEqual({...initialState,results:action.payload});
  });
  it('should check for error', () => {
    const payLoad = { error: 'Country Data Failur' };
    const action = new fromData.CountryFailure(payLoad);
    const result = reducer(initialState, action);
    expect(result).toEqual({...initialState, error:action.payload.error});
  });
  it('getItems to be called', () => {
    const getItemsCalled = getItems(initialState);
    expect(getItemsCalled).toEqual(initialState.results);
  });
  it('getRegionDetails to be called', () => {
    const getDetailsCalled = getRegionDetails(initialState);
    expect(getDetailsCalled).toEqual(initialState.regions);
  });
});
