export interface Region {
    value: string;
    viewValue: string;
}
export interface Countries {
    name: string;
    capital: string;
    population: number;
    currencies: string;
    flag: string;
}
