import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, switchMap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { CountriesService } from '../../services/countries.service';
import * as DataActions from '../actions/data.actions';

import { Countries } from '../models/region';

@Injectable({
  providedIn:'root'
})
export class DataEffects {
  constructor(private actions: Actions, private dataService: CountriesService) {}

  @Effect()
  getCountires$ = this.actions.pipe(
    ofType(DataActions.REGION),
    map(action => (action as any).payload),
    switchMap((region) => {
      return this.dataService.getCountries(region).pipe(
        map(resp => new DataActions.CountrySuccess(resp as Countries[])),
        catchError(error =>
          of(new DataActions.CountryFailure({ error: error }))
        )
      );
    })
  );
}
