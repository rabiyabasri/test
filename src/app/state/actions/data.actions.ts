import { Action } from '@ngrx/store';
import { Countries } from '../models/region';

export const REGION = 'Select Region';
export const COUNTRY_SUCCESS = 'Country Data Done';
export const COUNTRY_FAILURE = 'Country Data Failure';


export class Region implements Action {
  readonly type = REGION;
  constructor(public payload: string) { }
}
export class CountrySuccess implements Action {
  readonly type = COUNTRY_SUCCESS;
  constructor(public payload: Countries[]) { }
}
export class CountryFailure implements Action {
  readonly type = COUNTRY_FAILURE;

  constructor(public payload: { error: any }) {}
}
export type ActionsUnion = Region | CountrySuccess | CountryFailure;
