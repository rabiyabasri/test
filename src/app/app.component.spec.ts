import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { RegionComponent } from './components/region/region.component';
import { CountryDetailsComponent } from './components/country-details/country-details.component';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyMaterialModule } from './material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { CountriesService } from './services/countries.service';
import {mockStore} from './mockStore';
import { Subject} from 'rxjs';
import { StoreModule, Store, Action } from '@ngrx/store';
import { reducers, metaReducers } from './state/reducers';
import * as fromData from './state/reducers/data.reducer';

describe('AppComponent', () => {
  const actions = new Subject<Action>();
  const states = new Subject<fromData.DataState>();
  const store = mockStore<fromData.DataState>({ actions, states });
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppComponent, RegionComponent, CountryDetailsComponent ],
      imports:[ HttpClientModule, BrowserAnimationsModule, MyMaterialModule, ReactiveFormsModule, AppRoutingModule, 
RouterTestingModule,RouterModule.forRoot([]), StoreModule.forRoot(reducers, { metaReducers }) ],
      providers: [ CountriesService, {provide: Store, useValue: store} ]
    }).compileComponents();
  }));
  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});
