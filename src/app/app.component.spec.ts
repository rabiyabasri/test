import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { TableComponent } from './components/table/table.component';

import { TableService } from './core/services/table.service';
import { PagerService } from './core/services/pager.service';
import { HttpClientModule } from '@angular/common/http';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        TableComponent
      ],
      imports: [HttpClientModule],
      providers: [TableService, PagerService]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'Test'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Test');
  }));
});
