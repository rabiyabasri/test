import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { TableService } from '../../core/services/table.service';
import { PagerService } from '../../core/services/pager.service';
import { Subscription } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit, OnDestroy {
  allItems: any[] = [];
  subscription: Subscription;
   // pager object
  pager: any = {};
    // paged items
  pagedItems: any[];
  showSuggestion: Boolean = false;
  foundAll: Boolean = false;
  numberList: any = [50, 100, 150, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200];
  counter: number;
  constructor(private service: TableService, private http: HttpClient, private pagerService: PagerService) {  }
  ngOnInit() {
    this.subscription = this.service.getTableData().subscribe((data) => {
      this.allItems = data;
       // initialize to page 1
       this.setPage(1);
    });
  }
  setPage(page: number) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.allItems.length, page);

    // get current page of items
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);

    this.counter = this.pager.pageSize;
  }
  showDropDown() {
    this.showSuggestion = true;
    for (let i = 0; i < this.numberList.length; i++) {
      if (this.numberList[i] === this.allItems.length ) {
          this.foundAll = true;
          break;
      }
    }
  }
  numberClick(number: number) {
    this.pagedItems = this.allItems.slice(this.pager.startIndex, number);
    this.showSuggestion = false;
    this.counter = number;
  }
  getRowID(data: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/x-www-form-urlencoded'
      })};
    alert(data.id);
    const formData = { 'rowID': data.id, 'status': data.status};
    const apiUrl = '';
    this.http.post(apiUrl, formData, httpOptions).subscribe((res) => {
      console.log(res);
    },
    err => console.log(err)
    );
  }
  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
