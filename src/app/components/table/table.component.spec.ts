import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';
import { ElementRef } from '@angular/core';

import { TableComponent } from './table.component';
import { TableService } from '../../core/services/table.service';
import { PagerService } from '../../core/services/pager.service';
import { HttpClientModule } from '@angular/common/http';

export class MockElementRef extends ElementRef {}

describe('TableComponent', () => {
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;
  const activatedRoute = '';
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableComponent ],
      imports: [HttpClientModule],
      providers: [TableService, PagerService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('ngOnit called', () => {
      component.ngOnInit();
  });
  it('setPage called', () => {
    component.counter = 10;
    component.setPage(1);
    expect(component.counter).toBe(10);
  });
  it('showDropDown called', () => {
    component.showSuggestion = true;
    component.showDropDown();
    expect(component.showSuggestion).toBe(true);
  });
  it('numberClick Method called', () => {
    component.showSuggestion = true;
    component.numberClick(10);
    expect(component.showSuggestion).toBe(false);
  });
  it('getRowID Method called', () => {
    component.getRowID(10);
  });
  it('ngOnDestroy', inject([TableService], (service: TableService) => {
    spyOn(component, 'ngOnDestroy');
    component.ngOnDestroy();
    fixture.detectChanges();
    expect(component.ngOnDestroy).toHaveBeenCalled();
    }
  ));
  it('acronym', () => {
    fixture.detectChanges();
    spyOn(component.subscription, 'unsubscribe');
    component.ngOnDestroy();
    expect(component.subscription.unsubscribe).toHaveBeenCalled();
  });
});
