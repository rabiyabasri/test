import { Component, OnInit, OnDestroy} from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-country-details',
  templateUrl: './country-details.component.html',
  styleUrls: ['./country-details.component.scss']
})
export class CountryDetailsComponent implements OnInit, OnDestroy{
  selectedCountry: any;
  countDetailsSubscription: Subscription;
  constructor(private activatedRoute: ActivatedRoute) {  }
  ngOnInit() {
    this.countDetailsSubscription = this.activatedRoute.queryParamMap.subscribe(data =>{
      this.selectedCountry = JSON.parse(data.get('query'));
    });
  }
  ngOnDestroy() {
    this.countDetailsSubscription.unsubscribe();
  }
}
