import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CountryDetailsComponent } from './country-details.component';
import { RouterTestingModule } from '@angular/router/testing';
import { Router, RouterEvent, NavigationEnd } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Subject, ReplaySubject } from 'rxjs';
import { HttpClientModule } from '@angular/common/http';
import { MyMaterialModule } from '../../material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { CountriesService } from '../../services/countries.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

const eventSubject = new ReplaySubject<RouterEvent>(1);
const routerMock = {
  navigate: jasmine.createSpy('navigate'),
  events: eventSubject.asObservable(),
  url : 'home'
};
export class MockActiveRouteClass {
  queryParamMap = new Subject<any>();
}
describe('CountryDetailsComponent', () => {
  let component: CountryDetailsComponent;
  let fixture: ComponentFixture<CountryDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountryDetailsComponent ],
      imports : [RouterTestingModule, HttpClientModule,
        MyMaterialModule,
        ReactiveFormsModule, BrowserAnimationsModule],
      providers: [{ provide: Router, useValue: routerMock }, 
        { provide : ActivatedRoute, useClass : MockActiveRouteClass}, CountriesService ]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
