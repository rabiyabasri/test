import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RegionComponent } from './region.component';
import { CountryDetailsComponent } from '../country-details/country-details.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MyMaterialModule } from '../../material.module';
import { CountriesService } from '../../services/countries.service';
import {mockStore} from '../../mockStore';
import { Subject, Observable, of, ReplaySubject} from 'rxjs';
import { StoreModule, Store, Action, select } from '@ngrx/store';
import { reducers, metaReducers } from '../../state/reducers';
import * as fromData from '../../state/reducers/data.reducer';
import { RouterTestingModule } from '@angular/router/testing';
import { Router, RouterEvent, NavigationEnd } from '@angular/router';
const RouterStub = {
  navigate: jasmine.createSpy('navigate')
};
const eventSubject = new ReplaySubject<RouterEvent>(1);
const routerMock = {
  navigate: jasmine.createSpy('navigate'),
  events: eventSubject.asObservable(),
  url : 'home'
};
describe('RegionComponent', () => {
  let component: RegionComponent;
  let fixture: ComponentFixture<RegionComponent>;
  const actions = new Subject<Action>();
  const states = new Subject<fromData.DataState>();
  const store = mockStore<fromData.DataState>({ actions, states });
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegionComponent, CountryDetailsComponent ],
      imports: [HttpClientModule,
        BrowserAnimationsModule,
        MyMaterialModule,
        ReactiveFormsModule,RouterTestingModule,
        StoreModule.forRoot(reducers, { metaReducers })],
      providers: [
        CountriesService, {provide: Store, useValue: store},
        { provide: Router, useValue: routerMock }
      ]
    })
    .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(RegionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('getRegion to be called', () => {
    component.getRegion('asia');
  });
  it('getCountry to be called', () => {
    component.getCountry('asia');
    fixture.detectChanges();
  });
});
