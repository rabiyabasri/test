import { Component, OnInit, OnDestroy } from '@angular/core';
import { Region } from '../../state/models/region';
import { CountriesService } from '../../services/countries.service';
import { Store, select } from '@ngrx/store';
import * as fromRoot from '../../state/reducers';
import * as Actions from '../../state/actions/data.actions';
import { Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { map, skip } from 'rxjs/operators';
@Component({
  selector: 'app-region',
  templateUrl: './region.component.html',
  styleUrls: ['./region.component.scss']
})   
export class RegionComponent implements OnInit, OnDestroy {
  regions: any[] = [];
  countrySubscription: Subscription;
  regionSubscription: Subscription;
  countiresList: any[] = [];
  quotes: any;
  regionData: Observable<Region[]>;
  constructor(private countries: CountriesService, private store: Store<fromRoot.AppState>, private router: Router) { 
    this.regionData = store.pipe(select(fromRoot.getRegionData));
    this.quotes = store.pipe(select(fromRoot.getAllItems),
    skip(1),
    map(data =>
      data.map(resp => resp)
    ));
  }
  ngOnInit() {
    this.countrySubscription = this.quotes.subscribe(newData => {
      this.countiresList = newData;
    });
    this.regionSubscription = this.regionData.subscribe(region =>{
      this.regions = region;
    });
  }
  getRegion(region: string) {
    this.store.dispatch(new Actions.Region(region));
  }
  getCountry(country: any) {
    this.router.navigate(['/countryDetails'], { queryParams: { query: JSON.stringify(country) } });
  }
  ngOnDestroy() {
    if(this.countrySubscription){
      this.countrySubscription.unsubscribe();
    }
    if(this.regionSubscription){
      this.regionSubscription.unsubscribe();
    }
  }
}
