import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Region } from '../state/models/region';

@Injectable({
  providedIn: 'root'
})
export class CountriesService {

  constructor(private http: HttpClient) { }
  
  getCountries(region: string): Observable<any> {  
    return this.http.get<Region[]>(environment.apiURL + `/${region}`);  
  }    
}
