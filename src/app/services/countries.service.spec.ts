import { TestBed, inject } from '@angular/core/testing';

import { CountriesService } from './countries.service';
import { Subject, Observable, of} from 'rxjs';
import { StoreModule, Store, Action, select } from '@ngrx/store';
import { reducers, metaReducers } from '../state/reducers';
import * as fromData from '../state/reducers/data.reducer';

import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { environment } from '../../environments/environment';

describe('CountriesService', () => {
  let httpMock: HttpTestingController;
  const actions = new Subject<Action>();
  const states = new Subject<fromData.DataState>();
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, StoreModule.forRoot(reducers, { metaReducers })],
      providers: [ CountriesService ]
    });
  });
  beforeEach(() => {
    httpMock = TestBed.get(HttpTestingController);
  });
  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpMock.verify();
  });
  it('should be created', inject([CountriesService], (service: CountriesService) => {
    expect(service).toBeTruthy();
  }));
 
  describe('getCountries', () => {
    const region =  'asia';
    it('getCountries(region)', inject(
      [CountriesService],
      (service: CountriesService) => {
        service.getCountries(region).subscribe(defaultContent => {
          expect(defaultContent).toBe('Test');
        });
        const req = httpMock.expectOne(
          environment.apiURL + `/${region}`,
          'should match api URL'
        );
        expect(req.request.method).toBe('GET');
        req.flush('Test');
      }
    ));
  });
});
