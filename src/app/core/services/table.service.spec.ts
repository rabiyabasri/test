import { TestBed, inject } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
// import { HttpClientModule } from '@angular/common/http';
import { TableService } from './table.service';

describe('TableService', () => {
  let httpMock: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TableService]
    });
  });
  beforeEach(() => {
    httpMock = TestBed.get(HttpTestingController);
  });
  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpMock.verify();
  });
  it('should be created', inject([TableService], (service: TableService) => {
    expect(service).toBeTruthy();
  }));
  it('getTableData() called', inject([TableService],
    (service: TableService) => {
      service.getTableData().subscribe(defaultContent => {
        expect(defaultContent).toBe('Test');
      });
      const req = httpMock.expectOne(
        '../../../assets/json/sample_data.json',
        'should match api URL'
      );
      expect(req.request.method).toBe('GET');
      req.flush('Test');
    }
  ));
});
