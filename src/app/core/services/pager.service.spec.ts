import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { PagerService } from './pager.service';

describe('PagerService', () => {
  let httpMock: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PagerService]
    });
  });
  beforeEach(() => {
    httpMock = TestBed.get(HttpTestingController);
  });
  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpMock.verify();
  });
  it('should be created', inject([PagerService], (pagerService: PagerService) => {
    expect(pagerService).toBeTruthy();
  }));
  // getPager unit test
  describe('getPager', () => {
    const totalItems = 4;
    const startPage = 1;
    const endPage = 10;
    const currentPage = 1;
    const pageSize = 10;
    const totalPages = 10;
    const startIndex = 1;
    const endIndex = 10;
    const pages = 10;
    it('getPager', inject([PagerService], (pagerService: PagerService) => {
      spyOn(pagerService, 'getPager').and.returnValues({
        totalItems: totalItems,
        currentPage: currentPage,
        pageSize: pageSize,
        totalPages: totalPages,
        startPage: startPage,
        endPage: endPage,
        startIndex: startIndex,
        endIndex: endIndex,
        pages: pages
      });
      pagerService.getPager(totalItems, currentPage, pageSize);
      // expect(pagerService.getPager).toHaveBeenCalled();
      expect(pagerService.getPager).toHaveBeenCalledWith(totalItems, currentPage, pageSize);
    }
    ));
    it('getPager ', inject([PagerService], (pagerService: PagerService) => {
      const result = pagerService.getPager(totalItems, currentPage, pageSize);
      expect(result).toEqual({totalItems: 4, currentPage: 1, pageSize: 10, totalPages: 1, startPage: 1, endPage: 1,
        startIndex: 0, endIndex: 3, pages: [1]});
    }
    ));

    it('getPager ', inject([PagerService], (pagerService: PagerService) => {
      const result = pagerService.getPager(112, 5, 10);
      expect(result).toEqual({totalItems: 112, currentPage: 5, pageSize: 10, totalPages: 12, startPage: 1,
        endPage: 10, startIndex: 40, endIndex: 49, pages: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]});
    }
    ));
    it('getPager check Current Page for 0', inject([PagerService], (pagerService: PagerService) => {
      const result = pagerService.getPager(112, 0, 10);
      expect(result.currentPage).toEqual(1);
    }
    ));
    it('getPager check Current Page for Greater than pages', inject([PagerService], (pagerService: PagerService) => {
      const result = pagerService.getPager(110, 13, 10);
      expect(result.currentPage).toEqual(11);
    }
    ));
    it('getPager check total Page less than 10', inject([PagerService], (pagerService: PagerService) => {
      const result = pagerService.getPager(80, 1, 10);
      expect(result.endPage).toEqual(8);
    }
    ));
    it('getPager check total Page greater than 10', inject([PagerService], (pagerService: PagerService) => {
      const result = pagerService.getPager(800, 1, 10);
      expect(result.endPage).toEqual(10);
    }
    ));
  });
});
