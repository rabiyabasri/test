import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class TableService {
  constructor(private _http: HttpClient) { }
  getTableData(): Observable<any> {
    return this._http.get('../../../assets/json/sample_data.json');
  }
}
